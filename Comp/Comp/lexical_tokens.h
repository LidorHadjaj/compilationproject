#pragma once

#include <stdint.h> 
#include <stdio.h>

extern FILE *yyin, *yyout;

/*********************************************************************************************/


typedef enum
{
	ID_TOK,
	INTEGER_TOK,
	FLOAT_TOK,
	FLOAT_KW_TOK,
	INT_KW_TOK,
	IF_TOK,
	RETURN_TOK,
	ADD_TOK,
	MUL_TOK,
	LESSER_TOK,
	LESSER_EQUAL_TOK,
	EQUAL_TOK,
	NOT_EQUAL_TOK,
	GREATER_TOK,
	GREATER_EQUAL_TOK,
	ASSIGN_TOK,
	VOID_TOK,
	COMMA_TOK,
	COLON_TOK,
	SEMICOLON_TOK,
	OPEN_SQUARE_BRACKET_TOK,
	CLOSE_SQUARE_BRACKET_TOK,
	OPEN_CURL_TOK,
	CLOSE_CURL_TOK,
	OPEN_REGULAR_TOK,
	CLOSE_REGULAR_TOK,
	EOF_TOK
}token_kind_t;

/*********************************************************************************************/

typedef struct
{
	token_kind_t token_kind;
	char *lexeme;
	uint32_t line_number;
}token_t;

/*********************************************************************************************/

typedef struct Node
{
	token_t *tokensArray;
	struct Node *prev;
	struct Node *next;
} Node;

/*********************************************************************************************/

void create_and_store_token(token_kind_t token_kind,
	char *lexeme,
	uint32_t line_number);

/*********************************************************************************************/

token_t* next_token();

/*********************************************************************************************/

token_t* back_token();

/*********************************************************************************************/

void reset_token_list();

/*********************************************************************************************/

void free_token_list();

/*********************************************************************************************/

void print_token(token_kind_t token_kind, char *lexeme, uint32_t line_number);

/*********************************************************************************************/

uint8_t match(token_kind_t token_kind);

/*********************************************************************************************/

void nullify_token_list();

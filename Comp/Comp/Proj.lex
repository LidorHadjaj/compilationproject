%option noyywrap
%option yylineno


%{

#include "lexical_tokens.h"
#include "Parser.h"

FILE *parse_file = NULL;

%}






DIGIT         [0-9]
ALPHA_LOW	  [a-z]
ALPHA_HIGH    [A-Z]
ALPHA         [a-zA-Z]
ALPHA_DIGIT   [0-9A-Za-z]

INTEGER (0|([1-9]+[0-9]*))
EXPONENT (([1-9]+[0-9]*))
FLOAT {INTEGER}\.{INTEGER}+[eE][-+]?{EXPONENT}+

COMMENT \/\*([^*]|[\r\n]|(\*+([^*\/]|[\r\n])))*\*\/+

IDENTIFIER {ALPHA_LOW}({ALPHA}|{DIGIT})*("_"({ALPHA}|{DIGIT})+)*

ADDITION "+"
MULTIPLICATION "*"

LESSER "<"
LESSER_EQUAL "<="
EQUAL "=="
NOT_EQUAL "!="
GREATER ">"
GREATER_EQUAL ">="

ASSIGN "="



%%

{COMMENT}						{}

{FLOAT}						    {print_token(FLOAT_TOK, yytext, yyget_lineno());
								 create_and_store_token(FLOAT_TOK, yytext, yyget_lineno());
								 return 1;}

{INTEGER}						{print_token(INTEGER_TOK, yytext, yyget_lineno());
								 create_and_store_token(INTEGER_TOK, yytext, yyget_lineno());
								 return 1;}
							     

"void"							{print_token(VOID_TOK, yytext, yyget_lineno());
								 create_and_store_token(VOID_TOK, yytext, yyget_lineno());
								 return 1;}

"int" 							{print_token(INT_KW_TOK, yytext, yyget_lineno());
								 create_and_store_token(INT_KW_TOK, yytext, yyget_lineno());
								 return 1;}

"float"							{print_token(FLOAT_KW_TOK, yytext, yyget_lineno());
								 create_and_store_token(FLOAT_KW_TOK, yytext, yyget_lineno());
								 return 1;}

"if"							{print_token(IF_TOK, yytext, yyget_lineno());
								 create_and_store_token(IF_TOK, yytext, yyget_lineno());
								 return 1;}

"return"						{print_token(RETURN_TOK, yytext, yyget_lineno());
								 create_and_store_token(RETURN_TOK, yytext, yyget_lineno());
								 return 1;}






{ADDITION}						{print_token(ADD_TOK, yytext, yyget_lineno());
								 create_and_store_token(ADD_TOK, yytext, yyget_lineno());
								 return 1;}

{MULTIPLICATION}				{print_token(MUL_TOK, yytext, yyget_lineno());
								 create_and_store_token(MUL_TOK, yytext, yyget_lineno());
								 return 1;}


{LESSER}						{print_token(LESSER_TOK, yytext, yyget_lineno());
								 create_and_store_token(LESSER_TOK, yytext, yyget_lineno());
								 return 1;}

{LESSER_EQUAL}					{print_token(LESSER_EQUAL_TOK, yytext, yyget_lineno());
								 create_and_store_token(LESSER_EQUAL_TOK, yytext, yyget_lineno());
								 return 1;}

{EQUAL}							{print_token(EQUAL_TOK, yytext, yyget_lineno());
								 create_and_store_token(EQUAL_TOK, yytext, yyget_lineno());
								 return 1;}

{NOT_EQUAL}						{print_token(NOT_EQUAL_TOK, yytext, yyget_lineno());
								 create_and_store_token(NOT_EQUAL_TOK, yytext, yyget_lineno());
								 return 1;}

{GREATER}						{print_token(GREATER_TOK, yytext, yyget_lineno());
								 create_and_store_token(GREATER_TOK, yytext, yyget_lineno());
								 return 1;}

{GREATER_EQUAL}					{print_token(GREATER_EQUAL_TOK, yytext, yyget_lineno());
								 create_and_store_token(GREATER_EQUAL_TOK, yytext, yyget_lineno());
								 return 1;}

{ASSIGN}  						{print_token(ASSIGN_TOK, yytext, yyget_lineno());
								 create_and_store_token(ASSIGN_TOK, yytext, yyget_lineno());
								 return 1;}


","								{print_token(COMMA_TOK, yytext, yyget_lineno());
								 create_and_store_token(COMMA_TOK, yytext, yyget_lineno());
								 return 1;}

":"								{print_token(COLON_TOK, yytext, yyget_lineno());
								 create_and_store_token(COLON_TOK, yytext, yyget_lineno());
								 return 1;}

";"								{print_token(SEMICOLON_TOK, yytext, yyget_lineno());
								 create_and_store_token(SEMICOLON_TOK, yytext, yyget_lineno());
								 return 1;}

"["								{print_token(OPEN_SQUARE_BRACKET_TOK, yytext, yyget_lineno());
								 create_and_store_token(OPEN_SQUARE_BRACKET_TOK, yytext, yyget_lineno());
								 return 1;}

"]"								{print_token(CLOSE_SQUARE_BRACKET_TOK, yytext, yyget_lineno());
								 create_and_store_token(CLOSE_SQUARE_BRACKET_TOK, yytext, yyget_lineno());
								 return 1;}

"{"								{print_token(OPEN_CURL_TOK, yytext, yyget_lineno());
								 create_and_store_token(OPEN_CURL_TOK, yytext, yyget_lineno());
								 return 1;}

"}"								{print_token(CLOSE_CURL_TOK, yytext, yyget_lineno());
								 create_and_store_token(CLOSE_CURL_TOK, yytext, yyget_lineno());
								 return 1;}

"("								{print_token(OPEN_REGULAR_TOK, yytext, yyget_lineno());
								 create_and_store_token(OPEN_REGULAR_TOK, yytext, yyget_lineno());
								 return 1;}

")"								{print_token(CLOSE_REGULAR_TOK, yytext, yyget_lineno());
								 create_and_store_token(CLOSE_REGULAR_TOK, yytext, yyget_lineno());
								 return 1;}


[ \t\n]  				        {}

{IDENTIFIER}                    {print_token(ID_TOK, yytext, yyget_lineno());
								 create_and_store_token(ID_TOK, yytext, yyget_lineno()); 
								 return 1;}

.								{fprintf(yyout, "Character '%c' in line: {%d} does not begin any legal token in the language.\n", *yytext,  yyget_lineno()); 
							     return 1;}




<<EOF>> 						{print_token(EOF_TOK, yytext, yyget_lineno());
								 create_and_store_token(EOF_TOK, "", yyget_lineno()); 
								 return 0;}


%%



int main(int argc, char **argv)
{

	int i = 0;
	int j = 0;
	
	// First file analysis
	yyin = fopen("C:\\temp\\test1.txt", "r");
	yyout = fopen("C:\\temp\\test1_204553507_204609861_315857177_lex.txt", "w");
	parse_file = fopen("C:\\temp\\test1_204553507_204609861_315857177_syntactic.txt", "w" );

	if (yyout == NULL) 
	{
		yyout = stdout;
	}
	while(yylex() != 0) { i++;}

	reset_token_list();
	parse();
	
	fclose(parse_file);
	fclose(yyout);
	fclose(yyin);
	

	
	// Second file analysis
	yyin = fopen("C:\\temp\\test2.txt", "r");
	yyout = fopen("C:\\temp\\test2_204553507_204609861_315857177_lex.txt", "w");
	parse_file = fopen("C:\\temp\\test2_204553507_204609861_315857177_syntactic.txt", "w" );

	if (yyout == NULL) 
	{
		yyout = stdout;
	}
	
	nullify_token_list();

	yyrestart(yyin);
	yyset_lineno(1);
	while(yylex() != 0) {j++;}
	reset_token_list();
	parse();

	fclose(parse_file);
	fclose(yyout);
	fclose(yyin);


	free_token_list();

}

#pragma once

#include "lexical_tokens.h"
#include "Parser.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

token_t *t;


void parse();

/*********************************************************************************************/

void parse_PROG();

/*********************************************************************************************/

void parse_GLOBAL_VARS();

/*********************************************************************************************/

void parse_GLOBAL_VARS_TAG();

/*********************************************************************************************/

void parse_VAR_DEC();

/*********************************************************************************************/

void parse_TYPE();

/*********************************************************************************************/

void parse_VAR_DEC_TAG();

/*********************************************************************************************/

void parse_DIM_SIZES();

/*********************************************************************************************/

void parse_DIM_SIZES_TAG();

/*********************************************************************************************/

void parse_FUNC_PREDEFS();

/*********************************************************************************************/

void parse_FUNC_PREDEF_TAG();

/*********************************************************************************************/

void parse_FUNC_PROTOTYPE();

/*********************************************************************************************/

void parse_RETURNED_TYPE();

/*********************************************************************************************/

void parse_PARAMS();

/*********************************************************************************************/

void parse_PARAMS_LIST();

/*********************************************************************************************/

void parse_PARAMS_LIST_TAG();

/*********************************************************************************************/

void parse_PARAM();

/*********************************************************************************************/

void parse_PARAM_TAG();

/*********************************************************************************************/

void parse_FUNC_FULL_DEFS();

/*********************************************************************************************/

void parse_FUNC_WITH_BODY();

/*********************************************************************************************/

void parse_FUNC_FULL_DEFS_TAG();

/*********************************************************************************************/

void parse_COMP_STMT();

/*********************************************************************************************/

void parse_STMT_LIST();

/*********************************************************************************************/

void parse_STMT();

/*********************************************************************************************/

void parse_STMT_TAG();

/*********************************************************************************************/

void parse_IF_STMT();

/*********************************************************************************************/

void parse_TERM();

/*********************************************************************************************/

void parse_TERM_TAG();

/*********************************************************************************************/

void parse_FACTOR();

/*********************************************************************************************/

void parse_FACTOR_TAG();

/*********************************************************************************************/

void parse_CONDITION();

/*********************************************************************************************/

void parse_EXPR();

/*********************************************************************************************/

void parse_EXPR_TAG();

/*********************************************************************************************/

void parse_STMT_LIST_TAG();

/*********************************************************************************************/

void parse_VAR_DEC_LIST();

/*********************************************************************************************/

void parse_CALL();

/*********************************************************************************************/

void parse_ARGS();

/*********************************************************************************************/

void parse_ARG_LIST();

/*********************************************************************************************/

void parse_ARG_LIST_TAG();

/*********************************************************************************************/

void parse_RETURN_STMT();

/*********************************************************************************************/

void parse_VAR();

/*********************************************************************************************/

void parse_VAR_TAG();

/*********************************************************************************************/

void parse_EXPR_LIST();

/*********************************************************************************************/

void parse_EXPR_LIST_TAG();

/*********************************************************************************************/

void handle_parsing_error(token_kind_t *follow,
						  uint8_t follow_size);

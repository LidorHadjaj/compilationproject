#include "lexical_tokens.h"

#define TOKEN_ARRAY_SIZE 1000

extern FILE *parse_file;


int currentIndex = 0;
Node* currentNode = NULL;

/*********************************************************************************************/

void reset_token_list()
{
	currentIndex = 0;
	while (currentNode->prev != NULL)
	{
		currentNode = currentNode->prev;
	}
}

/*********************************************************************************************/

token_t* back_token()
{
	currentIndex--;
	return &currentNode->tokensArray[currentIndex];
}

/*********************************************************************************************/

token_t* next_token()
{
	token_t *temp = &currentNode->tokensArray[currentIndex];
	currentIndex++;

	return temp;
}

/*********************************************************************************************/

void free_token_list()
{

}

/*********************************************************************************************/

const char* get_token_name(token_kind_t t)
{
	switch (t)
	{
	case ID_TOK: return "ID\0";
	case INTEGER_TOK: return "INTEGER\0";
	case FLOAT_TOK: return "FLOAT\0";
	case FLOAT_KW_TOK: return "FLOAT_KW\0";
	case INT_KW_TOK: return "INT_KW\0";
	case IF_TOK: return "IF\0";
	case RETURN_TOK: return "RETURN\0";
	case ADD_TOK: return "ADD\0";
	case MUL_TOK: return "MUL\0";
	case LESSER_TOK: return "LESSER\0";
	case LESSER_EQUAL_TOK: return "LESSER_EQUAL\0";
	case EQUAL_TOK: return "EQUAL_KW\0";
	case NOT_EQUAL_TOK: return "NOT_EQUAL_KW\0";
	case GREATER_TOK: return "GREATER_KW\0";
	case GREATER_EQUAL_TOK: return "GREATER_EQUAL\0";
	case ASSIGN_TOK: return "ASSIGN\0";
	case VOID_TOK: return "VOID\0";
	case COMMA_TOK: return "COMMA\0";
	case COLON_TOK: return "COLON\0";
	case SEMICOLON_TOK: return "SEMICOLON\0";
	case OPEN_SQUARE_BRACKET_TOK: return "OPEN_SQUARE_BRACKET\0";
	case CLOSE_SQUARE_BRACKET_TOK: return "CLOSE_SQUARE_BRACKET\0";
	case OPEN_CURL_TOK: return "OPEN_CURL\0";
	case CLOSE_CURL_TOK: return "CLOSE_CURL\0";
	case OPEN_REGULAR_TOK: return "OPEN_REGULAR\0";
	case CLOSE_REGULAR_TOK: return "CLOSE_REGULAR\0";
	case EOF_TOK: return "EOF\0";

	default: return "ERROR\0";
	}
}

/*********************************************************************************************/

void print_token(token_kind_t token_kind, char *lexeme, uint32_t line_number)
{
	fprintf(yyout, 
			"Token of type '{%s}', lexeme: '{%s}', found in line: {%d}\n", 
			get_token_name(token_kind),
			lexeme,
			line_number);

}

/*********************************************************************************************/

void create_and_store_token(token_kind_t token_kind, char *lexeme, uint32_t line_number)
{
	int length = strlen(lexeme) + 1;

	// case 1: there is still no tokens in the storage.
	if (currentNode == NULL)
	{
		currentNode = (Node*)malloc(sizeof(Node));

		if (currentNode == NULL)
		{
			fprintf(yyout, "\nUnable to allocate memory! \n");
			exit(0);
		}
		currentNode->tokensArray = (token_t*)calloc(sizeof(token_t), TOKEN_ARRAY_SIZE);
		if (currentNode->tokensArray == NULL)
		{
			fprintf(yyout, "\nUnable to allocate memory! \n");
			exit(0);
		}
		currentNode->prev = NULL;
		currentNode->next = NULL;
	}

	// case 2: at least one token exsits in the storage.
	else
		// the array (the current node) is full, need to allocate a new node
		if (currentIndex == TOKEN_ARRAY_SIZE - 1)
		{
			currentIndex = 0;
			currentNode->next = (Node*)malloc(sizeof(Node));

			if (currentNode == NULL)
			{
				fprintf(yyout, "\nUnable to allocate memory! \n");
				exit(0);
			}
			currentNode->next->prev = currentNode;
			currentNode = currentNode->next;
			currentNode->tokensArray = (token_t*)calloc(sizeof(token_t), TOKEN_ARRAY_SIZE);

			if (currentNode->tokensArray == NULL)
			{
				fprintf(yyout, "\nUnable to allocate memory! \n");
				exit(0);
			}
			currentNode->next = NULL;
		}

	// the array (the current node) is not full
		else
		{
			currentIndex++;
		}


	currentNode->tokensArray[currentIndex].token_kind = token_kind;
	currentNode->tokensArray[currentIndex].line_number = line_number;

	currentNode->tokensArray[currentIndex].lexeme = (char*)malloc(sizeof(char)*length);
#ifdef _WIN32
	strcpy_s(currentNode->tokensArray[currentIndex].lexeme, length, lexeme);
#else
	strcpy(currentNode->tokensArray[currentIndex].lexeme, lexeme);
#endif		
}

/*********************************************************************************************/

uint8_t match(token_kind_t token_kind)
{
	token_t *t = next_token();
	uint8_t result = 0;

	if (t->token_kind == token_kind)
	{
		result = 1;
	}
	else
	{
		fprintf(parse_file, "Expected: token '%s' at line %d,\n", get_token_name(token_kind), t->line_number);
		fprintf(parse_file, "Actual token: '%s', lexeme �%s�\n", get_token_name(t->token_kind), t->lexeme);
	}

	return result;
}

/*********************************************************************************************/

void nullify_token_list()
{
	currentIndex = 0;
	currentNode = NULL;
}
#include "lexical_tokens.h"
#include "Parser.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

token_t *t;

extern FILE *parse_file;


void parse()
{
	parse_PROG();
	match(EOF_TOK);
}

/*********************************************************************************************/

void parse_PROG()
{
	fprintf(parse_file, "Rule (PROG -> GLOBAL_VARS FUNC_PREDEFS FUNC_FULL_DEFS)\n");

	parse_GLOBAL_VARS();

	parse_FUNC_PREDEFS();

	parse_FUNC_FULL_DEFS();
}

/*********************************************************************************************/

void parse_GLOBAL_VARS()
{
	fprintf(parse_file, "Rule (GLOBAL_VARS -> VAR_DEC GLOBAL_VARS_TAG)\n");

	parse_VAR_DEC();
	parse_GLOBAL_VARS_TAG();
}

/*********************************************************************************************/

void parse_GLOBAL_VARS_TAG() 
{
	token_kind_t follow[] = { 0 };
	uint32_t counter = 0;

	while (t->token_kind != SEMICOLON_TOK &&
		   t->token_kind != OPEN_REGULAR_TOK)
	{
		t = next_token();
		counter++;
	}

	switch (t->token_kind)
	{

	case SEMICOLON_TOK:
		fprintf(parse_file, "Rule (GLOBAL_VARS_TAG -> GLOBAL_VARS)\n");
		while(counter)
		{ 
			t = back_token(); 
			counter--;
		}
		parse_GLOBAL_VARS();
		break;
	case OPEN_REGULAR_TOK:
		fprintf(parse_file, "Rule (GLOBAL_VARS_TAG -> epsilon)\n");
		while (counter)
		{
			t = back_token();
			counter--;
		}
		break;
	default:

		break;
	}
}

/*********************************************************************************************/

void parse_VAR_DEC()
{
	token_kind_t follow[] = { 0 };

	fprintf(parse_file, "Rule (VAR_DEC -> TYPE id VAR_DEC_TAG)\n");
	
	parse_TYPE();
	match(ID_TOK);
	parse_VAR_DEC_TAG();
}

/*********************************************************************************************/

void parse_TYPE()
{
	token_kind_t follow[] = { ID_TOK };
	t = next_token();

	switch (t->token_kind)
	{
	case FLOAT_KW_TOK :
		fprintf(parse_file, "Rule (TYPE -> float)\n");
		break;
	case INT_KW_TOK:
		fprintf(parse_file, "Rule (TYPE -> int)\n");
		break;
	default:
		fprintf(parse_file,
			    "Expected token of type '{FLOAT_KW_TOK, INT_KW_TOK}' at line: '{%d}' , Actual token of type: '{%s}', lexeme: '{%s}'.\n",
			     t->line_number,
			     get_token_name(t->token_kind),
			     t->lexeme);
		handle_parsing_error(follow, sizeof(follow));
		break;
	}
}

/*********************************************************************************************/

void parse_VAR_DEC_TAG()
{
	token_kind_t follow[] = { VOID_TOK, INT_KW_TOK, FLOAT_KW_TOK  };
	t = next_token();

	switch (t->token_kind)
	{
	case SEMICOLON_TOK:
		fprintf(parse_file, "Rule (VAR_DEC_TAG -> ;)\n");
		break;
	case OPEN_SQUARE_BRACKET_TOK:
		fprintf(parse_file, "Rule (VAR_DEC_TAG -> [DIM_SIZES];)\n");
		parse_DIM_SIZES();
		match(CLOSE_SQUARE_BRACKET_TOK);
		match(SEMICOLON_TOK);
		break;
	case OPEN_REGULAR_TOK:
		t = back_token();
		t = back_token();
		t = back_token();
		break;
	default:
		fprintf(parse_file,
			"Expected token of type '{SEMICOLON_TOK, OPEN_SQUARE_BRACKET_TOK}' at line: '{%d}' , Actual token of type: '{%s}', lexeme: '{%s}'.\n",
			t->line_number,
			get_token_name(t->token_kind),
			t->lexeme);
		handle_parsing_error(follow, sizeof(follow));
		break;
	}
}

/*********************************************************************************************/

void parse_DIM_SIZES()
{
	token_kind_t follow[] = { CLOSE_SQUARE_BRACKET_TOK };
	t = next_token();

	switch (t->token_kind)
	{
	case INTEGER_TOK:
		fprintf(parse_file, "rule (DIM_SIZES -> int_num DIM_SIZES_TAG)\n");
		parse_DIM_SIZES_TAG();

		break;
	default:
		fprintf(parse_file,
			"Expected token of type '{INTEGER_TOK}' at line: '{%d}' , Actual token of type: '{%s}', lexeme: '{%s}'.\n",
			t->line_number,
			get_token_name(t->token_kind),
			t->lexeme);
		handle_parsing_error(follow, sizeof(follow));
		break;
	}
}

/*********************************************************************************************/

void parse_DIM_SIZES_TAG()
{
	token_kind_t follow[] = { CLOSE_SQUARE_BRACKET_TOK };
	t = next_token();

	switch (t->token_kind)
	{
	case CLOSE_SQUARE_BRACKET_TOK:
		fprintf(parse_file, "rule (DIM_SIZES_TAG -> epsilon)\n");
		t = back_token();
		break;
	case COMMA_TOK:
		fprintf(parse_file, "rule (DIM_SIZES_TAG -> ,DIM_SIZES)\n");
		parse_DIM_SIZES();
		break;
	default:
		fprintf(parse_file,
			"Expected token of type '{COMMA_TOK, CLOSE_SQUARE_BRACKET_TOK}' at line: '{%d}' , Actual token of type: '{%s}', lexeme: '{%s}'.\n",
			t->line_number,
			get_token_name(t->token_kind),
			t->lexeme);
		handle_parsing_error(follow, sizeof(follow));
		break;
	}
}

/*********************************************************************************************/

void parse_FUNC_PREDEFS()
{
	fprintf(parse_file, "Rule (FUNC_PREDEFS -> FUNC_PROTOTYPE; FUNC_PREDEF_TAG)\n");

	parse_FUNC_PROTOTYPE();
	match(SEMICOLON_TOK);
	parse_FUNC_PREDEF_TAG();
}

/*********************************************************************************************/

void parse_FUNC_PROTOTYPE()
{
	fprintf(parse_file, "Rule (FUNC_PROTOTYPE -> RETURNED_TYPE id(PARAMS))\n");

	parse_RETURNED_TYPE();
	match(ID_TOK);
	match(OPEN_REGULAR_TOK);
	parse_PARAMS();
	match(CLOSE_REGULAR_TOK);
}

/*********************************************************************************************/

void parse_RETURNED_TYPE()
{
	token_kind_t follow[] = { ID_TOK };
	t = next_token();

	switch (t->token_kind)
	{
	case FLOAT_KW_TOK:

		fprintf(parse_file, "Rule (RETURNED_TYPE -> float)\n");
		break;

	case INT_KW_TOK:

		fprintf(parse_file, "Rule (RETURNED_TYPE -> int)\n");
		break;

	case VOID_TOK:
		fprintf(parse_file, "Rule (RETURNED_TYPE -> void)\n");
		break;

	default:
		fprintf(parse_file,
			"Expected token of type '{FLOAT_KW_TOK, INT_KW_TOK, VOID_TOK}' at line: '{%d}' , Actual token of type: '{%s}', lexeme: '{%s}'.\n",
			t->line_number,
			get_token_name(t->token_kind),
			t->lexeme);
		handle_parsing_error(follow, sizeof(follow));
		break;
	}
}

/*********************************************************************************************/

void parse_PARAMS()
{
	token_kind_t follow[] = { CLOSE_REGULAR_TOK };
	t = next_token();

	switch (t->token_kind)
	{
	case CLOSE_REGULAR_TOK:
		fprintf(parse_file, "Rule (PARAMS -> epsilon)\n");
		t = back_token();
		break;

	case INT_KW_TOK:
		fprintf(parse_file, "Rule (PARAMS -> PARAMS_LIST)\n");
		t = back_token();
		parse_PARAMS_LIST();
		break;

	case FLOAT_KW_TOK:
		fprintf(parse_file, "Rule (PARAMS -> PARAMS_LIST)\n");
		t = back_token();
		parse_PARAMS_LIST();
		break;

	default:
		fprintf(parse_file,
			"Expected token of type '{FLOAT_KW_TOK, INT_KW_TOK, CLOSE_REGULAR_TOK}' at line: '{%d}' , Actual token of type: '{%s}', lexeme: '{%s}'.\n",
			t->line_number,
			get_token_name(t->token_kind),
			t->lexeme);
		handle_parsing_error(follow, sizeof(follow));
		break;
	}
}

/*********************************************************************************************/

void parse_PARAMS_LIST()
{
	fprintf(parse_file, "Rule (PARAMS_LIST -> PARAM PARAMS_LIST_TAG)\n");

	parse_PARAM();
	parse_PARAMS_LIST_TAG();
}

/*********************************************************************************************/

void parse_PARAM()
{
	fprintf(parse_file, "Rule (PARAM -> TYPE id PARAM_TAG)\n");

	parse_TYPE();
	match(ID_TOK);
	parse_PARAM_TAG();
}

/*********************************************************************************************/

void parse_PARAM_TAG()
{
	t = next_token();

	switch (t->token_kind)
	{
	case OPEN_SQUARE_BRACKET_TOK:
		fprintf(parse_file, "Rule (PARAM_TAG -> [DIM_SIZES])\n");
		parse_DIM_SIZES();
		match(CLOSE_SQUARE_BRACKET_TOK);
		break;
	default:
		fprintf(parse_file, "Rule (PARAM_TAG -> epsilon)\n");
		t = back_token();
		break;
	}
}

/*********************************************************************************************/

void parse_PARAMS_LIST_TAG()
{
	token_kind_t follow[] = { CLOSE_REGULAR_TOK };
	t = next_token();

	switch (t->token_kind)
	{
	case COMMA_TOK:
		fprintf(parse_file, "Rule (PARAMS_LIST_TAG -> ,PARAMS_LIST)\n");
		parse_PARAMS_LIST();
		break;

	case CLOSE_REGULAR_TOK:
		fprintf(parse_file, "Rule (PARAMS_LIST_TAG -> epsilon)\n");
		t = back_token();
		break;

	default:
		fprintf(parse_file,
			"Expected token of type '{COMMA_TOK, CLOSE_REGULAR_TOK}' at line: '{%d}' , Actual token of type: '{%s}', lexeme: '{%s}'.\n",
			t->line_number,
			get_token_name(t->token_kind),
			t->lexeme);
		handle_parsing_error(follow, sizeof(follow));		
		break;
	}
}

/*********************************************************************************************/

void parse_FUNC_PREDEF_TAG()
{
	uint32_t counter = 0;

	while (t->token_kind != SEMICOLON_TOK &&
		   t->token_kind != OPEN_CURL_TOK)
	{
		t = next_token();
		counter++;
	}

	switch (t->token_kind)
	{
	case SEMICOLON_TOK:
		fprintf(parse_file, "Rule (FUNC_PREDEF_TAG -> FUNC_PREDEFS)\n");
		while (counter)
		{
			t = back_token();
			counter--;
		}
		parse_FUNC_PREDEFS();
		break;
	case OPEN_CURL_TOK:
		fprintf(parse_file, "Rule (FUNC_PREDEF_TAG -> epsilon)\n");
		while (counter)
		{
			t = back_token();
			counter--;
		}
		break;
	default:

		break;
	}
}

/*********************************************************************************************/

void parse_FUNC_FULL_DEFS()
{
	fprintf(parse_file, "Rule (FUNC_FULL_DEFS -> FUNC_WITH_BODY FUNC_FULL_DEFS_TAG)\n");

	parse_FUNC_WITH_BODY();

	parse_FUNC_FULL_DEFS_TAG();
}

/*********************************************************************************************/

void parse_FUNC_WITH_BODY()
{
	fprintf(parse_file, "Rule (FUNC_WITH_BODY -> FUNC_PROTOTYPE COMP_STMT)\n");

	parse_FUNC_PROTOTYPE();
	parse_COMP_STMT();
}

/*********************************************************************************************/

void parse_COMP_STMT()
{
	match(OPEN_CURL_TOK);
	fprintf(parse_file, "Rule (COMP_STMT -> {VAR_DEC_LIST STMT_LIST})\n");
	parse_VAR_DEC_LIST();
	parse_STMT_LIST();
	match(CLOSE_CURL_TOK);
}

/*********************************************************************************************/

void parse_VAR_DEC_LIST()
{
	t = next_token();

	// A variant decleration always
	// starts with int/float keywords
	if (t->token_kind != INT_KW_TOK &&
		t->token_kind != FLOAT_KW_TOK)
	{
		fprintf(parse_file, "Rule (VAR_DEC_LIST -> epsilon)\n");

		t = back_token();
		return;
	}

	fprintf(parse_file, "Rule (VAR_DEC_LIST -> VAR_DEC VAR_DEC_LIST)\n");

	t = back_token();
	parse_VAR_DEC();
	parse_VAR_DEC_LIST();
}

/*********************************************************************************************/

void parse_STMT_LIST()
{
	fprintf(parse_file, "Rule (STMT_LIST -> STMT STMT_LIST_TAG)\n");

	parse_STMT();
	parse_STMT_LIST_TAG();
}

/*********************************************************************************************/

void parse_STMT()
{
	token_kind_t follow[] = { CLOSE_CURL_TOK, SEMICOLON_TOK };
	t = next_token();

	switch (t->token_kind)
	{
	case OPEN_CURL_TOK:
		fprintf(parse_file, "Rule (STMT -> COMP_STMT)\n");
		t = back_token();
		parse_COMP_STMT();
		break;
	case IF_TOK:
		fprintf(parse_file, "Rule (STMT -> IF_STMT)\n");
		t = back_token();
		parse_IF_STMT();
		break;
	case RETURN_TOK:
		fprintf(parse_file, "Rule (STMT -> RETURN_STMT)\n");
		t = back_token();
		parse_RETURN_STMT();
		break;
	case ID_TOK:
		fprintf(parse_file, "Rule (STMT -> id STMT_TAG)\n");
		parse_STMT_TAG();
		break;
	default:
		fprintf(parse_file,
			"Expected token of type '{OPEN_REGULAR_TOK, FLOAT_TOK, INTEGER_TOK, ID_TOK}' at line: '{%d}' , Actual token of type: '{%s}', lexeme: '{%s}'.\n",
			t->line_number,
			get_token_name(t->token_kind),
			t->lexeme);
		handle_parsing_error(follow, sizeof(follow));
		break;
	}
}

/*********************************************************************************************/

void parse_STMT_TAG()
{
	token_kind_t follow[] = { SEMICOLON_TOK, CLOSE_CURL_TOK };
	t = next_token();

	if (t->token_kind == ASSIGN_TOK)
	{
		fprintf(parse_file, "Rule (STMT_TAG -> =EXPR)\n");
		parse_EXPR();
	}
	else if (t->token_kind == OPEN_SQUARE_BRACKET_TOK)
	{
		fprintf(parse_file, "Rule (STMT_TAG -> [EXPR_LIST] = EXPR)\n");
		parse_EXPR_LIST();
		match(CLOSE_SQUARE_BRACKET_TOK);
		match(ASSIGN_TOK);
		parse_EXPR();
	}
	else if (t->token_kind == OPEN_REGULAR_TOK)
	{
		parse_ARGS;
		match(CLOSE_REGULAR_TOK);
	}
	else
	{
		fprintf(parse_file,
			"Expected token of type '{ASSIGN_TOK, OPEN_SQUARE_BRACKET_TOK, OPEN_REGULAR_TOK}' at line: '{%d}' , Actual token of type: '{%s}', lexeme: '{%s}'.\n",
			t->line_number,
			get_token_name(t->token_kind),
			t->lexeme);
		handle_parsing_error(follow, sizeof(follow));
	}
}

/*********************************************************************************************/

void parse_RETURN_STMT()
{
	match(RETURN_TOK);

	t = next_token();
	if (t->token_kind != SEMICOLON_TOK)
	{
		fprintf(parse_file, "Rule (RETURN_STMT -> return EXPR)\n");
		t = back_token();
		parse_EXPR();
	}
	else
	{
		fprintf(parse_file, "Rule (RETURN_STMT -> return)\n");
	}
}

/*********************************************************************************************/

void parse_IF_STMT()
{
	fprintf(parse_file, "Rule (IF_STMT -> if(CONDITION) STMT)\n");

	match(IF_TOK);
	match(OPEN_REGULAR_TOK);
	parse_CONDITION();
	match(CLOSE_REGULAR_TOK);
	parse_STMT();
}

/*********************************************************************************************/

void parse_CONDITION()
{
	token_kind_t follow[] = { OPEN_REGULAR_TOK };
	fprintf(parse_file, "Rule (CONDITION -> EXPR rel_op EXPR)\n");

	parse_EXPR();
	t = next_token();
	
	if (t->token_kind == LESSER_TOK ||
		t->token_kind == LESSER_EQUAL_TOK ||
		t->token_kind == EQUAL_TOK ||
		t->token_kind == GREATER_EQUAL_TOK ||
		t->token_kind == GREATER_TOK ||
		t->token_kind == NOT_EQUAL_TOK)
	{
		// OK!
	}
	else
	{
		fprintf(parse_file,
			"Expected token of type '{OPEN_REGULAR_TOK, FLOAT_TOK, INTEGER_TOK, ID_TOK}' at line: '{%d}' , Actual token of type: '{%s}', lexeme: '{%s}'.\n",
			t->line_number,
			get_token_name(t->token_kind),
			t->lexeme);
		handle_parsing_error(follow, sizeof(follow));
		return;
	}

	parse_EXPR();
}

/*********************************************************************************************/

void parse_EXPR()
{
	fprintf(parse_file, "Rule (EXPR -> TERM EXPR_TAG)\n");

	parse_TERM();
	parse_EXPR_TAG();
}

/*********************************************************************************************/

void parse_TERM()
{
	fprintf(parse_file, "Rule (TERM -> FACTOR TERM_TAG)\n");

	parse_FACTOR();
	parse_TERM_TAG();
}

/*********************************************************************************************/

void parse_FACTOR()
{
	token_kind_t follow[] = { SEMICOLON_TOK };
	t = next_token();

	switch (t->token_kind)
	{
	case OPEN_REGULAR_TOK:
		fprintf(parse_file, "Rule (FACTOR -> (EXPR)\n");
		parse_EXPR();
		match(CLOSE_REGULAR_TOK);
		break;
	case FLOAT_TOK:
		fprintf(parse_file, "Rule (FACTOR -> float_num)\n");
		break;
	case INTEGER_TOK:
		fprintf(parse_file, "Rule (FACTOR -> int_num)\n");
		break;
	case ID_TOK:
		fprintf(parse_file, "Rule (FACTOR -> id FACTOR_TAG)\n");
		parse_FACTOR_TAG();
		break;
	default:
		fprintf(parse_file,
			"Expected token of type '{OPEN_REGULAR_TOK, FLOAT_TOK, INTEGER_TOK, ID_TOK}' at line: '{%d}' , Actual token of type: '{%s}', lexeme: '{%s}'.\n",
			t->line_number,
			get_token_name(t->token_kind),
			t->lexeme);
		handle_parsing_error(follow, sizeof(follow));
		break;
	}
}

/*********************************************************************************************/

void parse_FACTOR_TAG()
{
	t = next_token();
	if (t->token_kind == OPEN_REGULAR_TOK)
	{
		fprintf(parse_file, "Rule (FACTOR_TAG -> (ARGS)\n");
		parse_ARGS();
		match(CLOSE_REGULAR_TOK);
	}
	else
	{
		fprintf(parse_file, "Rule (FACTOR_TAG -> VAR_TAG)\n");
		t = back_token();
		parse_VAR_TAG();
	}
}

/*********************************************************************************************/

void parse_VAR()
{
	fprintf(parse_file, "Rule (VAR -> id VAR_TAG)\n");
	match(ID_TOK);
	parse_VAR_TAG();
}

/*********************************************************************************************/

void parse_VAR_TAG()
{
	t = next_token();

	if (t->token_kind == OPEN_SQUARE_BRACKET_TOK)
	{
		fprintf(parse_file, "Rule (VAR_TAG -> [EXPR_LIST])\n");
		parse_EXPR_LIST();
		match(CLOSE_SQUARE_BRACKET_TOK);
	}
	else
	{
		t = back_token();
		fprintf(parse_file, "Rule (VAR_TAG -> epsilon)\n");
	}
}

/*********************************************************************************************/

void parse_EXPR_LIST()
{
	fprintf(parse_file, "Rule (EXPR_LIST -> EXPR EXPR_LIST_TAG)\n");
	
	parse_EXPR();
	parse_EXPR_LIST_TAG();
}

/*********************************************************************************************/

void parse_EXPR_LIST_TAG()
{
	t = next_token();
	if(t->token_kind == COMMA_TOK)
	{
		fprintf(parse_file, "Rule (EXPR_LIST_TAG -> EXPR_LIST)\n");
		parse_EXPR_LIST();
	}
	else
	{
		fprintf(parse_file, "Rule (EXPR_LIST_TAG -> epsilon)\n");
		t = back_token();
	}
}

/*********************************************************************************************/

void parse_CALL()
{
	fprintf(parse_file, "Rule (CALL -> id(ARGS))\n");

	match(ID_TOK);
	match(OPEN_REGULAR_TOK);
	parse_ARGS();
	match(CLOSE_REGULAR_TOK);
}

/*********************************************************************************************/

void parse_ARGS()
{
	t = next_token();

	switch (t->token_kind)
	{
	case CLOSE_CURL_TOK:
		fprintf(parse_file, "Rule (ARGS -> epsilon)\n");
		t = back_token();
		break;
	default:
		fprintf(parse_file, "Rule (ARGS -> ARG_LIST)\n");
		t = back_token();
		parse_ARG_LIST();
		break;
	}
}

/*********************************************************************************************/

void parse_ARG_LIST()
{
	fprintf(parse_file, "Rule (ARG_LIST -> EXPR ARG_LIST_TAG\n");
	parse_EXPR();
	parse_ARG_LIST_TAG();
}

/*********************************************************************************************/

void parse_ARG_LIST_TAG()
{
	t = next_token();

	switch (t->token_kind)
	{
	case COMMA_TOK:
		fprintf(parse_file, "Rule (ARG_LIST_TAG -> , ARG_LIST\n");
		parse_ARG_LIST();
		break;
	default:
		fprintf(parse_file, "Rule (ARG_LIST_TAG -> epsilon)\n");
		t = back_token();
		break;
	}
}

/*********************************************************************************************/

void parse_TERM_TAG()
{
	t = next_token();

	switch (t->token_kind)
	{
	case MUL_TOK:
		fprintf(parse_file, "Rule (TERM_TAG -> * TERM)\n");
		parse_TERM();
		break;
	default:
		fprintf(parse_file, "Rule (TERM_TAG -> epsilon)\n");
		t = back_token();
		break;
	}
}

/*********************************************************************************************/

void parse_EXPR_TAG()
{
	t = next_token();

	if (t->token_kind == ADD_TOK)
	{
		fprintf(parse_file, "Rule (EXPR_TAG -> + EXPR)\n");
		parse_EXPR();
	}
	else
	{
		fprintf(parse_file, "Rule (EXPR_TAG -> epsilon)\n");
		t = back_token();
	}
}

/*********************************************************************************************/

void parse_STMT_LIST_TAG()
{
	token_kind_t follow[] = { CLOSE_CURL_TOK };
	t = next_token();

	if (t->token_kind == SEMICOLON_TOK)
	{
		fprintf(parse_file, "Rule (STMT_LIST_TAG -> ;STMT_LIST)\n");
		parse_STMT_LIST();
	}
	else if (t->token_kind == CLOSE_CURL_TOK)
	{
		t = back_token();
		fprintf(parse_file, "Rule (STMT_LIST_TAG -> epsilon)\n");
	}
	else
	{
		fprintf(parse_file,
			"Expected token of type '{SEMICOLON_TOK, CLOSE_CURL_TOK}' at line: '{%d}' , Actual token of type: '{%s}', lexeme: '{%s}'.\n",
			t->line_number,
			get_token_name(t->token_kind),
			t->lexeme);
		handle_parsing_error(follow, sizeof(follow));
	}
}

/*********************************************************************************************/

void parse_FUNC_FULL_DEFS_TAG()
{
	t = next_token();
	if (t->token_kind != EOF_TOK)
	{
		fprintf(parse_file, "Rule (FUNC_FULL_DEFS_TAG -> FUNC_FULL_DEFS)\n");
		t = back_token();
		parse_FUNC_FULL_DEFS();
	}
	else
	{
		fprintf(parse_file, "Rule (FUNC_FULL_DEFS_TAG -> epsilon)\n");
		t = back_token();
	}
}

/*********************************************************************************************/

void handle_parsing_error(token_kind_t *follow, uint8_t follow_size)
{
	uint8_t counter = 0;

	while (1)
	{
		// In case we reached the EOF
		// just exit...there is nothing left to parse...
		if (t->token_kind == EOF_TOK)
		{
			exit(1);
		}
		else
		{
			for (counter = 0; counter < follow_size; counter++)
			{
				if (follow[counter] == t->token_kind)
				{
					t = back_token();
					return;
				}
			}
		}

		// Try this at the end because
		// the first coming might also 
		// be a follow
		t = next_token();
	}
}